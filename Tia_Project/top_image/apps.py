from django.apps import AppConfig


class TopImageConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'top_image'
