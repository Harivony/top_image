from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django import forms


"""class UserForm()"""


class LoginForm(forms.Form):
    username = forms.CharField(max_length=63, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': "Nom d'utilisateur"}))

    password = forms.CharField(max_length=63, widget=forms.PasswordInput(
        attrs={'class': 'form-control', 'placeholder': "Mot de passe"}))


"""    def __int__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        icons = getattr(self.Meta, 'icons', dict())

        self.username.
"""


class SignupForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = get_user_model()
        fields = ('first_name', 'last_name', 'username', 'email', 'profile_photo', 'role')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['class'] = "form-control"
        self.fields['last_name'].widget.attrs['class'] = "form-control"
        self.fields['username'].widget.attrs['class'] = "form-control"
        self.fields['email'].widget.attrs['class'] = "form-control"
        self.fields['profile_photo'].widget.attrs['class'] = "form-control"
        self.fields['role'].widget.attrs['class'] = "form-select"
        self.fields['password1'].widget.attrs['class'] = "form-control"
        self.fields['password2'].widget.attrs['class'] = "form-control"
