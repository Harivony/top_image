from django.contrib import admin
from .models import *


class RtlAdmin(admin.ModelAdmin):
    list_display = ('rtl_name', 'rtl_zone', 'rtl_contact')


class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'profile_photo', 'role')


admin.site.register(Rtl, RtlAdmin)
admin.site.register(User, UserAdmin)
