from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader

from django.http import JsonResponse
from django.conf import settings
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.views.generic import View

from . import forms


class LoginPage(View):
    form_class = forms.LoginForm
    template_name = 'accounts/login.html'

    def get(self, request):
        form = self.form_class()
        message = ''
        return render(request, self.template_name, context={'form': form, 'message': message})

    def post(self, request):
        form = self.form_class(request.POST)
        message = ''
        if form.is_valid():
            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            if user is not None:
                login(request, user)
                return redirect('index')
            else:
                return JsonResponse({'error': 'Invalid log in'})
        return render(request, self.template_name, context={'form': form, 'message': message})


def login_page(request):
    form = forms.LoginForm()
    message = ''
    if request.method == 'POST':
        form = forms.LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            if user is not None:
                login(request, user)
                return redirect('index')
            else:
                message = f'Identifiants incorrects.'
    return render(request, 'accounts/login.html', context={ 'form' : form, 'message' : message })


def logout_user(request):
    logout(request)
    return redirect('login')


def signup_page(request):
    form = forms.SignupForm()
    message = ''
    if request.method == 'POST':
        form = forms.SignupForm(request.POST, request.FILES)
        if form.is_valid():
            user = form.save()
            # auto-login user
            login(request, user)
            message = 'Création de compte avec succès.'
            return redirect('index')
    return render(request, 'accounts/signup.html', context={'form': form})


@login_required()
def index(request):
    return render(request, 'dashboard/index.html')


def mes_conges(request):
    return render(request, 'dashboard/mes-conges.html')
