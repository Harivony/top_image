console.log("Js login")

let classes = (classes) => document.getElementsByClassName(classes);
let id = (id) => document.getElementById(id);

let form = id("form_login"),
    username = id("username"),
    password = id("password"),
    error_message = classes("error"),
    successIcon = classes("success-icon"),
    failureIcon = classes("failure-icon");

form.addEventListener("submit", (e) => {
    e.preventDefault();

    engine(username, 0, 'Username cannot be blank')
});

let engine = (id, serial, message) => {
    if(id.value.trim() == ''){
        error_message[serial].innerHTML = message;
        failureIcon[serial].style.opacity = "1";
        successIcon[serial].style.opacity = "0";
    }
    else {
        error_message[serial].innerHTML = "";
        failureIcon[serial].style.opacity = "0";
        successIcon[serial].style.opacity = "1";
    }
};


    



