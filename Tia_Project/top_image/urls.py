from django.urls import path
from django.contrib.auth.views import LoginView


from . import views

urlpatterns = [
    path('index', views.index, name='index'),
    path('mesconges', views.mes_conges, name='mesconges'),
    path('login', views.LoginPage.as_view(), name='login'),
    path('logout', views.logout_user, name='logout'),
    path('signup', views.signup_page, name='signup'),

]