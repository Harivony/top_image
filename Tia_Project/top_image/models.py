from django.db import models
from django.contrib.auth.models import AbstractUser


# Creer un model user
class User(AbstractUser):
    RTL = 'RTL'
    TDR = 'TDR'

    ROLE_CHOICES = (
        (RTL, 'Responsable'),
        (TDR,'Agent'),
    )

    profile_photo = models.FileField(upload_to='profile_image', verbose_name='Photo de profil')

    role = models.CharField(max_length=30, choices=ROLE_CHOICES, verbose_name='role')


class Rtl(models.Model):
    rtl_name = models.CharField(max_length=50, blank=True, verbose_name="Nom du RTL")
    rtl_zone = models.CharField(max_length=50, blank=True, verbose_name="Zone d'intervention")
    rtl_contact = models.CharField(max_length=10, blank=True, verbose_name="Contact")

    def __str__(self):
        return self.rtl_name
